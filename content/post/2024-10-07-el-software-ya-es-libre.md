---
title: "El software ya es libre"
date: 2024-10-07
description: ""
category:
  - General
tags: [free software]
---
Hoy se celebra el día mundial del software libre. Pero por si acaso no lo sabes, ya te lo digo yo, el software es libre.

Cuando comenzó a desarrollarse el mundo de la informática, los ordenadores eran aparatos muy caros que casi no cabían en una habitación. Un par de fabricantes se repartían el mercado de ese hardware, porque en aquel momento el software no valía nada por sí mismo. En ese momento el software era libre.

La cosa siguió avanzando, los ordenadores empezaron a cambiar de tamaño, cada vez eran más pequeños y su capacidad de procesamiento más grande. Llegamos a la década de los 70, aparece el PC, el personal computer, la posibilidad de llevar los ordenadores a los hogares. Al mercado del hardware empiezan a llegar más fabricantes, el software poco a poco empieza a ser compatible entre equipos y entonces se vuelve importante.

De repente alguien piensa que el negocio ya no está tanto en el hardware sino en el software, así que decide cogerlo y meterlo en cajas para venderlo. Junto con unos textos amplios y de tamaño de letra muy pequeña que establecen las condiciones bajo las que, una vez comprado, puedes hacer uso del mismo. Porque lo compraste, pero no es tuyo. Solo tienes una licencia de uso.

Desde ese momento llega una época oscura, en la que el software ya no es libre. Pero igual que en los cómics de Astérix, hay una aldea de irreductibles galos que no están dispuestos a dejarse conquistar por el imperio. Son el movimiento del Software Libre.

Gracias a ellos el software resistió y no todo se volvió privativo. Pero fueron tiempos difíciles, porque el imperio intentó acabar con estos valientes galos de muchas formas.

Finalmente, y como bien sabemos por la historia, todos los grandes imperios acaban cayendo, o cambiando. Hoy en día, esas grandes corporaciones que querían acabar con el software libre, son las que más contribuyen a su desarrollo. *Cosas veredes*.

Vivimos ahora en un tiempo donde el hardware está en la nube y el software se utiliza para crear servicios, que es donde está el negocio. El software dejó de ser un fin para ser un medio y, por tanto, ya puede ser libre de nuevo. No hay mal que por bien no venga.

Pero si otra cosa nos enseñó la historia es que, de una forma u otra, todo se repite. Cuando un imperio cae, otro ya está empezando a crecer. Por eso ahora que el software vuelve a ser libre, no podemos bajar la guardia. En nuestras manos está lograr que esta situación perdure. No venció la filosofía, sino la conveniencia, y por lo tanto este *statu quo* es aún frágil.

En nuestras manos está hacer ver que este modelo funciona, que se puede hacer negocio liberando el software, que puede ser igual o incluso más seguro a pesar de que el código fuente sea público, que facilita y potencia mucho más la innovación. En definitiva, que es el modelo que nunca se debió abandonar.

Porque el software siempre fue libre.

--   
*[Editorial publicada en el portal Mancomún](https://mancomun.gal/es/novas/el-software-ya-es-libre/).*